from typing import Callable

from slack_bolt import App
from slack_bolt.adapter.socket_mode import SocketModeHandler

from scraperbox import get_answer


APP_TOKEN = "YOUR-APP-TOKEN"
BOT_TOKEN = "YOUR-BOT-TOKEN"
app = App(token=BOT_TOKEN)


@app.event("app_mention")
def mention_handler(body: dict, say: Callable):
    sender_id = f"<@{body.get('event', {}).get('user')}>"
    say(f"Let me check that for you {sender_id}")
    bot_id = body.get("event", {}).get("text").split()[0]
    message = body.get("event", {}).get("text")
    message = message.replace(bot_id, "").strip()
    answer = get_answer(message)
    say(answer)


if __name__ == "__main__":
    handler = SocketModeHandler(app, APP_TOKEN)
    handler.start()
