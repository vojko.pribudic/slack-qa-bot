import requests
from bs4 import BeautifulSoup


API_TOKEN = "YOUR-SCRAPERBOX-API-TOKEN"


def _get_json_response(query: str) -> dict:
    params = {
        "token": API_TOKEN,
        "q": query,
        "proxy_location": "gb",
        "return_html": "true",
    }
    resp = requests.get("https://api.scraperbox.com/google", params=params)
    return resp.json()


def get_answer(query: str) -> str:
    answer = "No idea how to answer that :("
    resp = _get_json_response(query)

    if "html" not in resp:
        return answer

    soup = BeautifulSoup(resp["html"], "html.parser")
    el = soup.find("div", class_="kno-rdesc")

    if not el:
        return answer

    return el.span.text
